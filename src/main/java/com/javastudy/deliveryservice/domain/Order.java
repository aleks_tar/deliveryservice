package com.javastudy.deliveryservice.domain;

import java.util.Date;

public class Order {
    private String number;
    private String type;
    private Client sender;
    private Client receiver;
    private Date deliveryDate;
    private String comments;

    public Order(String number, String type, Client sender, Client receiver,
            Date deliveryDate, String comments) {
        setNumber(number);
        setType(type);
        setSender(sender);
        setReceiver(receiver);
        setDeliveryDate(deliveryDate);
        setComments(comments);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Client getSender() {
        return sender;
    }

    public void setSender(Client sender) {
        this.sender = sender;
    }

    public Client getReceiver() {
        return receiver;
    }

    public void setReceiver(Client receiver) {
        this.receiver = receiver;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}
