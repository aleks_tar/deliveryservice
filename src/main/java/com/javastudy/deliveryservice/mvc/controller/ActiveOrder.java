package com.javastudy.deliveryservice.mvc.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.javastudy.deliveryservice.domain.Client;
import com.javastudy.deliveryservice.domain.Order;

@Controller
@RequestMapping("client/activeorder")
public class ActiveOrder {

    private Calendar defineCalendar() {
        return GregorianCalendar.getInstance();
    }

    private Order defineOrder1() {
        Client receiver = defineClientPetrov();
        Client sender = defineClientSidorov();
        Calendar calendar = defineCalendar();
        calendar.set(2015, 8, 5, 13, 0, 0);
        Date deliveryDate = calendar.getTime();
        return new Order("1", "Доставка наложенным платежом", sender, receiver,
                deliveryDate, "Никаких комментариев");
    }

    private Client defineClientSidorov() {
        return new Client("Сидоров Сергей Александрович",
                "г. Харьков, ул. Октябрьская, 78, кв. 115",
                "sidorrrr@gmail.com", "+380934567812");
    }

    private Client defineClientPetrov() {
        return new Client("Петров Андрей Иванович",
                "г. Киев, ул. Героев Севастополя, 15Б, кв. 11",
                "petrov.andrey@somemail.com", "+380671234567");
    }

    private Client defineClientSokolova() {
        return new Client("Соколова Анна Владимировна",
                "г. Николаев, пр. Космонавтов, 20В, кв. 15",
                "sokolov@nikolaev.ua", "+380937891325");
    }

    private Order defineOrder2() {
        Client receiver = defineClientPetrov();
        Client sender = defineClientSokolova();
        Calendar calendar = defineCalendar();
        calendar.set(2015, 8, 5, 13, 0, 0);
        Date deliveryDate = calendar.getTime();
        return new Order("2", "Доставка наложенным платежом", sender, receiver,
                deliveryDate, "Никаких комментариев");
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getActiveOrder(Model model, HttpServletRequest request) {
        Integer orderId = Integer.valueOf(request.getParameter("id"));
        Map<Integer, Order> orders = new TreeMap<Integer, Order>();
        orders.put(Integer.valueOf(1), defineOrder1());
        orders.put(Integer.valueOf(2), defineOrder2());

        addOrderToModel(model, orders.get(orderId));
        return "client/activeOrder";
    }

    private void addOrderToModel(Model model, Order order) {
        model.addAttribute("number", order.getNumber());
        model.addAttribute("type", order.getType());
        model.addAttribute("receiverName", order.getReceiver().getName());
        model.addAttribute("receiverAddress", order.getReceiver().getAddress());
        model.addAttribute("receiverEmail", order.getReceiver().getEmail());
        model.addAttribute("receiverPhone", order.getReceiver().getPhone());
        model.addAttribute("senderName", order.getSender().getName());
        model.addAttribute("senderAddress", order.getSender().getAddress());
        model.addAttribute("senderEmail", order.getSender().getEmail());
        model.addAttribute("senderPhone", order.getSender().getPhone());
        DateFormat dateFormat = SimpleDateFormat.getDateInstance();
        model.addAttribute("deliveryDate",
                dateFormat.format(order.getDeliveryDate()));
        DateFormat timeFormat = new SimpleDateFormat("HH:mm");
        model.addAttribute("deliveryTime",
                timeFormat.format(order.getDeliveryDate()));
        model.addAttribute("comments", order.getComments());
    }
}
