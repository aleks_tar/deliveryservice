<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Order information</title>
</head>
<body>
    <h1>Заказ # ${number}</h1>
    <table>
    <tr><td>Тип заказа:</td><td>${type}</td></tr>
    <tr><td>Наименование получателя:</td><td>${receiverName}</td></tr>
    <tr><td>Адрес получателя:</td><td>${receiverAddress}</td></tr>
    <tr><td>Имейл получателя:</td><td>${receiverEmail}</td></tr>
    <tr><td>Телефон получателя:</td><td>${receiverPhone}</td></tr>
    <tr><td>Наименование отправителя:</td><td>${senderName}</td></tr>
    <tr><td>Адрес отправителя:</td><td>${senderAddress}</td></tr>
    <tr><td>Имейл отправителя:</td><td>${senderEmail}</td></tr>
    <tr><td>Телефон отправителя:</td><td>${senderPhone}</td></tr>
    <tr><td>Дата доставки:</td><td>${deliveryDate}</td></tr>
    <tr><td>Время доставки:</td><td>${deliveryTime}</td></tr>
    <tr><td>Примечания:</td><td>${comments}</td></tr>
    </table>
</body>
</html>